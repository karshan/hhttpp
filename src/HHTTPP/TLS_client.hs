{-# LANGUAGE OverloadedStrings #-}

module HHTTPP.TLS_client where

import Network.TLS
import Data.X509.CertificateStore
import Data.Default.Class (def)
import Network.Socket (HostName)
import Network.TLS.Extra.Cipher (ciphersuite_all)
import System.IO (Handle)

connect :: Handle -> String -> IO Context
connect handle hostname =
  let params = default_client_params hostname
  in contextNew handle params

default_client_params :: HostName -> ClientParams
default_client_params hostname =
  let shared = default_shared
      c_hooks = default_client_hooks
      c_supported = default_client_supported
  in ClientParams Nothing (hostname, "") {- use SNI -} True Nothing shared c_hooks c_supported

default_shared :: Shared
default_shared =
  let creds = default_creds
      manager = default_manager
      cert_store = default_cert_store
      validation_cache = default_validation_cache
  in Shared creds manager cert_store validation_cache

default_creds :: Credentials
default_creds = Credentials []

-- TODO consider using hs-tls/.../SimpleClient.hs:sessionRef
default_manager :: SessionManager
default_manager = noSessionManager

default_cert_store :: CertificateStore
default_cert_store = makeCertificateStore []

default_validation_cache :: ValidationCache
default_validation_cache = def

default_client_hooks :: ClientHooks
default_client_hooks = def

default_client_supported :: Supported
default_client_supported =
  let versions = [ SSL2, SSL3, TLS10, TLS11, TLS12 ]
      ciphers = ciphersuite_all
      compressions = [nullCompression]
      hash_signatures = [(x, y) | x <- [ HashNone, HashMD5, HashSHA1, HashSHA224, HashSHA256, HashSHA384, HashSHA512 ],
                                  y <- [ SignatureAnonymous, SignatureRSA, SignatureDSS, SignatureECDSA ]]
  in Supported versions ciphers compressions hash_signatures True True True False
