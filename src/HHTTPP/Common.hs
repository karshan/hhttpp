{-# LANGUAGE OverloadedStrings #-}
module HHTTPP.Common where

import Data.Monoid
import Data.CaseInsensitive (CI, mk, original)
import Data.Maybe (fromMaybe)
import HHTTPP.Util (maybe_read)
import Debug.Trace
import Text.Parsec.ByteString (Parser)
import Text.Parsec
import Text.Parsec.Char
import Data.ByteString.Char8 (ByteString, append, pack, unpack)
import qualified Data.ByteString.Char8 as BS (map, take)
import Data.Char

data Header = Header (CI ByteString, ByteString) deriving (Show, Eq, Ord)

data CommonBody = CommonBody {
  headers :: [Header],
  body :: ByteString
} deriving (Show, Eq, Ord)

consume_spaces :: Parser ByteString
consume_spaces = fmap pack (many (char ' '))

consume_eol :: Parser ByteString
consume_eol = fmap pack (string "\r\n" <|> string "\n")

parse_msg_version :: Parser ByteString
parse_msg_version = fmap pack (many (noneOf " \r\n"))

parse_msg_header :: Parser Header
parse_msg_header =
  parse_msg_header_key >>= (\key ->
  parse_msg_header_val >>= (\val ->
  consume_eol >>
  return (Header (key, val))))

parse_msg_header_key :: Parser (CI ByteString)
parse_msg_header_key = fmap (mk . pack) (many (noneOf ":\r\n"))

parse_msg_header_val :: Parser ByteString
parse_msg_header_val = fmap pack (char ':' >> consume_spaces >> many (noneOf "\r\n"))

parse_chunked :: ByteString -> Parser ByteString
parse_chunked acc = do
  n <- (fromMaybe 0 . (maybe_read :: String -> Maybe Int) . ("0x" ++)) <$> many1 hexDigit
  consume_eol
  if n > 0 then
    parse_chunked =<< ((acc <>) . pack) <$> (count n anyToken <* consume_eol)
  else
    return acc

parse_msg_body :: [Header] -> Parser ByteString
parse_msg_body headers = if fmap (BS.map toLower) (lookupHeader "Transfer-Encoding" headers) == Just "chunked" then
      parse_chunked ""
    else
      fmap pack (count (fromMaybe 0 (get_content_length headers)) anyToken)

lookupHeader :: ByteString -> [Header] -> Maybe ByteString
lookupHeader key = lookup (mk key) . map (\(Header x) -> x)

get_content_length :: [Header] -> Maybe Int
get_content_length headers = maybe_read =<< fmap unpack (lookupHeader "Content-Length" headers)

parse_headers_and_body:: Parser ([Header], ByteString)
parse_headers_and_body =
  many parse_msg_header >>= (\headers ->
  consume_eol >>
  option "" (parse_msg_body headers) >>= (\body ->
  return (headers, body)))

instance Print_http Header where
  print_http (Header (key, value)) = append (original key) (append (append ": " value) (pack "\n"))

class Print_http a where
  print_http :: a -> ByteString
